import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import h5py

from sklearn.mixture import GaussianMixture
from scipy.interpolate import interp1d
from scipy.stats import gaussian_kde
from scipy.signal import find_peaks
from matplotlib.pyplot import figure

class kde_plus_gmm():
    def __init__(self,U,binbin,N):
               
        rsl = 500
        bins = binbin
        figure(figsize=(8.0, 4.0), dpi=rsl)
        
        U = U*1e4
        data = np.reshape(U,(1,-1))
        xx = np.linspace(np.min(data),np.max(data),700)
                
        # ============ kde ============  
        kernel = gaussian_kde(data,bw_method='scott') # !

        pks = find_peaks(kernel.evaluate(xx),height = 0.5)[0]
        xpk = [xx[jj] for jj in pks]
        nop = len(xpk)
        ypk = [kernel.evaluate(xx[jj]) for jj in pks]
        xpk_sorted = -np.sort(-np.array(xpk)) # in decsending order 
#         print(xpk_sorted)
        
        binval,binss,_ = plt.hist(data.T,bins,density=True,edgecolor='grey',facecolor='none')
        #plt.plot(xx,kernel.evaluate(xx),'k-',label='kernal density estimation(KDE)')
        #plt.scatter(xpk,ypk,c="r",s=80,marker="o",label='peaks of KDE')  
        
        # ============ gmm ============        
        
        X = np.array(U).T.reshape(-1,1)
        #mini = np.reshape(xpk_sorted[:N],(-1,1))
        wini = np.array([1/N for ik in range(N)]) # equal weights initially means_init = mini,  weights_init = wini,
        model = GaussianMixture( n_components = N, tol = 1e-4, weights_init = wini, random_state=0 ).fit(X)

        # compute the AIC and the BIC criterion 
        #AIC = [m.aic(X) for m in models]
        #BIC = [m.bic(X) for m in models]

        M_best = model#s[np.argmin(BIC)] # or AIC 
        
        g_means = M_best.means_
        g_cov = M_best.covariances_
        g_weights = M_best.weights_
        #N_best = N[np.argmin(BIC)]  ## best number of kernels 
        
        logprob = M_best.score_samples(xx.reshape(-1, 1))
        responsibilities = M_best.predict_proba(xx.reshape(-1, 1))
        pdf = np.exp(logprob)
        pdf_individual = responsibilities * pdf[:, np.newaxis]
             
        plt.plot(xx,pdf,'-',label='Gaussian Mixture Estimation')
        plt.plot(xx,pdf_individual,'--',label='individual Gaussian component')
        
        binmid = np.zeros(72)
        for i in range(70):
            binmid[i+1] = (binss[i]+binss[i+1])/2
        binmid[0] = np.min(data)
        binmid[71] = np.max(data)
        binvalnew = np.zeros(72)
        for i in range(70):
            binvalnew[i+1] = binval[i]
        binvalnew[0] = 0
        binvalnew[71] = 0
        f = interp1d(binmid, binvalnew)
        histogram = f(xx)
        #plt.plot(xx,histogram)
        
        #histogram = np.zeros(700)
        #for i in range(70):
        #    for j in range(10):
        #        histogram[10*i+j] = binval[i]  
        dev = np.linalg.norm(pdf-histogram)/np.linalg.norm(histogram)
        print(dev)
             
#       ini = np.reshape(xpk,(-1,1))
#         gmmModel = GaussianMixture(n_components=k, reg_covar=cov_th,tol=1e-6,max_iter=1000)
#                                    ,means_init=ini)
#                                    ,covariance_type='diag', reg_covar=3e-3)
#                                    , means_init=ini)
#         gmmModel.fit((np.array(U).T).reshape(-1,1))        
#         wgts=gmmModel.weights_
#         mu=gmmModel.means_
#         cor=gmmModel.covariances_ 
#         indx = np.arange(np.min(data),np.max(data),(np.max(data)-np.min(data))/500)
#         yy   = 0 * indx  
#         for ik in range(k):        
#             zz  = wgts[ik]/np.sqrt(2*3.1416)/cor[ik][0]*np.exp(-(indx-mu[ik])**2/2/cor[ik][0]**2)
#             yy += zz
#             plt.plot(indx,zz,label='Kernel (%d in %d)'%(ik+1,k))          
#         plt.plot(indx,yy,label='Synthetic p.d.f. ')

        # ============ summary ============
        #my_x_ticks = np.arange(0.4, 1.2, 0.1)
        #my_y_ticks = np.arange(0, 13, 1)
        #plt.xticks(my_x_ticks)
        #plt.yticks(my_y_ticks)
    
        plt.xlabel("Streamwise Velocity(*1e-4)",fontdict={'family' : 'Calibri', 'size':12})
        plt.ylabel("Frequncy",fontdict={'family' : 'Calibri', 'size':12})
        #plt.title("restart 010%s p.d.f. X#%d at Zlabel = %d"%(rst,jy,staz),fontdict={'family' : 'Calibri', 'size':12})
        #plt.legend(loc='upper left', prop={'family':'Calibri', 'size':10},frameon=False)
        
        #ax=plt.gca();# get the handle of the axis 
        #ax.spines['bottom'].set_linewidth(0.5);
        #ax.spines['left'].set_linewidth(0.5);
        #ax.spines['right'].set_linewidth(0.5);
        #ax.spines['top'].set_linewidth(0.5);
        
        plt.show()
        plt.close()     
        # ============          ============
        #              plot BIC 
        # ============          ============
        #figure(num=10, figsize=(7.0, 4.0), dpi=rsl)        
        #plt.plot(AIC)
        #plt.plot(BIC)
        #plt.show()
        #plt.close()

        #cxk1 = ' '.join(str(i) for i in np.array(xpk_sorted))
        #cxk2 = ' '.join(str(i) for i in means_g)
        #cxk3 = ' '.join(str(i) for i in cov_g)
        #cxk4 = ' '.join(str(i) for i in g_weights)
        #nopk = len(xpk_sorted)
        #nogk = len(g_weights)