import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import h5py

from sklearn.mixture import GaussianMixture
from scipy.stats import gaussian_kde
from scipy.signal import find_peaks
from matplotlib.pyplot import figure

class kde():
    def __init__(self,U,binbin):
               
        rsl = 500
        bins = binbin
        figure(figsize=(8.0, 4.0), dpi=rsl)
        
        data = np.reshape(U,(1,-1))
        xx = np.linspace(np.min(data),np.max(data),500)
                
        # ============ kde ============  
        kernel = gaussian_kde(data,bw_method='scott') # !

        pks = find_peaks(kernel.evaluate(xx),height = 0.5)[0]
        xpk = [xx[jj] for jj in pks]
        nop = len(xpk)
        ypk = [kernel.evaluate(xx[jj]) for jj in pks]
        xpk_sorted = -np.sort(-np.array(xpk)) # in decsending order 
#         print(xpk_sorted)
        
        plt.hist(data.T,bins,density=True,edgecolor='grey',facecolor='none')
        plt.plot(xx,kernel.evaluate(xx),'k-',label='kernal density estimation(KDE)')
        plt.scatter(xpk,ypk,c="r",s=80,marker="o",label='peaks of KDE')