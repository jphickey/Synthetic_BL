import numpy as np
import wire
import biotsavart_gaussian
from Bezier import Bezier

class Rep_eddy:
    
    def __init__(self,h,current,locx,locz,volume_corner1,volume_corner2):
        w1 = wire.Wire(path=wire.Wire.AlphapVortex(scale=h), discretization_length=0.02, current=-current).Rotate(axis=(1, 0, 0), deg=90).Translate((locx+1.2*h, 0, locz))
        wn1 = wire.Wire(path=wire.Wire.AlphanVortex(scale=h), discretization_length=0.02, current=-current).Rotate(axis=(1, 0, 0), deg=90).Translate((1.2*h, h, locz))

        w2 = wire.Wire(path=wire.Wire.AlphapVortex(scale=h-0.1), discretization_length=0.02, current=-current).Rotate(axis=(1, 0, 0), deg=90).Translate((locx+0.8*h, 0, locz))
        wn2 = wire.Wire(path=wire.Wire.AlphanVortex(scale=h-0.1), discretization_length=0.02, current=-current).Rotate(axis=(1, 0, 0), deg=90).Translate((locx+0.8*h, h-0.1, locz))

        w3 = wire.Wire(path=wire.Wire.AlphapVortex(scale=h-0.2), discretization_length=0.02, current=-current).Rotate(axis=(1, 0, 0), deg=90).Translate((locx+0.4*h, 0, locz))
        wn3 = wire.Wire(path=wire.Wire.AlphanVortex(scale=h-0.2), discretization_length=0.02, current=-current).Rotate(axis=(1, 0, 0), deg=90).Translate((locx+0.4*h, h-0.2, locz))

        w4 = wire.Wire(path=wire.Wire.AlphapVortex(scale=h-0.3), discretization_length=0.02, current=-current).Rotate(axis=(1, 0, 0), deg=90).Translate((locx, 0, locz))
        wn4 = wire.Wire(path=wire.Wire.AlphanVortex(scale=h-0.3), discretization_length=0.02, current=-current).Rotate(axis=(1, 0, 0), deg=90).Translate((locx, h-0.3, locz))

        w5 = wire.Wire(path=wire.Wire.AlphapVortex(scale=h-0.4), discretization_length=0.02, current=-current).Rotate(axis=(1, 0, 0), deg=90).Translate((locx-0.4*h, 0, locz))
        wn5 = wire.Wire(path=wire.Wire.AlphanVortex(scale=h-0.4), discretization_length=0.02, current=-current).Rotate(axis=(1, 0, 0), deg=90).Translate((locx-0.4*h, h-0.4, locz))

        w6 = wire.Wire(path=wire.Wire.AlphapVortex(scale=h-0.5), discretization_length=0.02, current=-current).Rotate(axis=(1, 0, 0), deg=90).Translate((locx-0.8*h, 0, locz))
        wn6 = wire.Wire(path=wire.Wire.AlphanVortex(scale=h-0.5), discretization_length=0.02, current=-current).Rotate(axis=(1, 0, 0), deg=90).Translate((locx-0.8*h, h-0.5, locz))

        w7 = wire.Wire(path=wire.Wire.AlphapVortex(scale=h-0.6), discretization_length=0.02, current=-current).Rotate(axis=(1, 0, 0), deg=90).Translate((locx-1.2*h, 0, locz))
        wn7 = wire.Wire(path=wire.Wire.AlphanVortex(scale=h-0.6), discretization_length=0.02, current=-current).Rotate(axis=(1, 0, 0), deg=90).Translate((locx-1.2*h, h-0.6, locz))
        
        sol1  = biotsavart_gaussian.BiotSavart(wire=w1)
        soln1  = biotsavart_gaussian.BiotSavart(wire=wn1)

        sol2  = biotsavart_gaussian.BiotSavart(wire=w2)
        soln2  = biotsavart_gaussian.BiotSavart(wire=wn2)

        sol3  = biotsavart_gaussian.BiotSavart(wire=w3)
        soln3  = biotsavart_gaussian.BiotSavart(wire=wn3)

        sol4  = biotsavart_gaussian.BiotSavart(wire=w4)
        soln4  = biotsavart_gaussian.BiotSavart(wire=wn4)

        sol5  = biotsavart_gaussian.BiotSavart(wire=w5)
        soln5  = biotsavart_gaussian.BiotSavart(wire=wn5)
        
        resolution = 0.05

        sol6  = biotsavart_gaussian.BiotSavart(wire=w6)
        soln6  = biotsavart_gaussian.BiotSavart(wire=wn6)

        sol7  = biotsavart_gaussian.BiotSavart(wire=w7)
        soln7  = biotsavart_gaussian.BiotSavart(wire=wn7)
        
        grid = np.mgrid[volume_corner1[0]:volume_corner2[0]:resolution, volume_corner1[1]:volume_corner2[1]:resolution]

        points = np.vstack(map(np.ravel, grid)).T
        points = np.hstack([points, np.zeros([len(points),1])])
        
        # calculate B field at given points
        B7  = sol7.CalculateB(points=points) + soln7.CalculateB(points=points)
        B1  = sol1.CalculateB(points=points) + soln1.CalculateB(points=points)
        B2 = sol2.CalculateB(points=points) + soln2.CalculateB(points=points)
        B3 = sol3.CalculateB(points=points) + soln3.CalculateB(points=points)
        B4 = sol4.CalculateB(points=points) + soln4.CalculateB(points=points)
        B5 = sol5.CalculateB(points=points) + soln5.CalculateB(points=points)
        B6 = sol6.CalculateB(points=points) + soln6.CalculateB(points=points)

        Babs7  = np.linalg.norm(B7, axis=1)
        Babs1  = np.linalg.norm(B1, axis=1)
        Babs2 = np.linalg.norm(B2, axis=1)
        Babs3 = np.linalg.norm(B3, axis=1)
        Babs4 = np.linalg.norm(B4, axis=1)
        Babs5 = np.linalg.norm(B5, axis=1)
        Babs6 = np.linalg.norm(B6, axis=1)
    
        # remove big values close to the wire
        cutoff = 0.0003
        B7[Babs7 > cutoff]   = [np.nan,np.nan,np.nan]
        B1[Babs1 > cutoff]   = [np.nan,np.nan,np.nan]
        B2[Babs2 > cutoff] = [np.nan,np.nan,np.nan]
        B3[Babs3 > cutoff] = [np.nan,np.nan,np.nan]
        B4[Babs4 > cutoff] = [np.nan,np.nan,np.nan]
        B5[Babs5 > cutoff] = [np.nan,np.nan,np.nan]
        B6[Babs6 > cutoff] = [np.nan,np.nan,np.nan]
        
        self.Ux=B7[:, 0]+B1[:, 0]+B2[:,0]+B3[:,0]+B4[:,0]+B5[:,0]+B6[:,0]
        self.Uy=B7[:, 1]+B1[:, 1]+B2[:,1]+B3[:,1]+B4[:,1]+B5[:,1]+B6[:,1]
        self.Uz=B7[:, 2]+B1[:, 2]+B2[:,2]+B3[:,2]+B4[:,2]+B5[:,2]+B6[:,2]